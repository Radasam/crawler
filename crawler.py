import gevent
from gevent import monkey, queue
monkey.patch_all()

import logging
import requests
import re
import datetime
from bs4 import BeautifulSoup

class Crawler:
    
    def __init__(self, domain:str, show_logs:bool, workers:int=1, max_depth: int=-1, page_limit:int=100, exclude = list(), retries:int=1):
        """Takes a url and finds all urls linked to that url in the same domain
           workers: set number of workers
           max_depth: set max depth of links -1 for unlimited
           exclude: exclude links which contain any of a set of strings e.g. .css

           methods:
            run(): run the crawler
            report(): print results to the console
            report_status(): print summary of requests
        """
        self.workers = workers
        self.visited_urls = []
        self.url_queue: queue.JoinableQueue
        self.domain = domain
        self.filter_items = ['https:','http:']
        self.exclude = exclude  
        self.dict_report = {}
        self.dict_status = {}
        self.max_depth = max_depth
        self.logger = logging.getLogger('crawler_log')
        self.__has_ran = False
        self.page_count = 0
        self.page_limit = page_limit
        self.retries = retries

        self.start_time = datetime.datetime.now().timestamp()

        if show_logs:
            logging.basicConfig(level=logging.INFO)
        else:
            logging.basicConfig(level=logging.ERROR)


    def __update_status_dict(self, status_code, url):
        if status_code in self.dict_status.keys():
            self.dict_status[status_code].append(url)
        else:
            self.dict_status[status_code] = [url]           


    def __clean_url(self, parent_url, url):
        if url == "":
            return url

        # use requests to resolve relatve urls
        # account for fragments, anything after # is a fragment
        # we still want to check if the url preceeding a fragment has been visited
        if '#' in url:
            cleaned_url = requests.compat.urljoin(parent_url, url.split('#')[0])
        else:
            cleaned_url = requests.compat.urljoin(parent_url, url)
        return cleaned_url


    def __update_report(self, source: str, target: str):
        if source in self.dict_report.keys():
            self.dict_report[source].append(target)
        else:
            self.dict_report[source] = [target]


    def __get_links(self):
        # code will exit when the queue is empty
        # here we need a while statement to loop through queue items
        while True:
            # Pull a url from the queue
            item : CrawlerQueueItem = self.url_queue.get()
            # if the user has specified a max depth, check if we have exceeded
            if(self.max_depth < 1 or item.depth < self.max_depth):
                # we want to be sure we remove the item from the queue even if we encounter a fail
                # hence we can use a try ... finally
                try:
                    self.logger.info(f"""Visiting {item.url}""")

                    # set user agent to common value to avoid being blocked as a bot
                    headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36'
                                , 'Accept': '*/*'
                                ,'Accept-Encoding': 'gzip, deflate, br'
                               ,'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8'
                               ,'Connection': 'keep-alive'
                               ,'Upgrade-Insecure-Requests': '1'}

                    if item.referrer_url != '':
                        headers['Referrer'] = item.referrer_url
                    
                    # Request the url as plain text
                    
                    s = requests.Session()
                    a = requests.adapters.HTTPAdapter(max_retries=self.retries)
                    s.mount('https://',a)
                    response = s.get(item.url, headers=headers)


                    self.__update_status_dict(response.status_code, item.url)

                    if response.status_code != 200:
                        self.logger.error(f"""Error fetching {item.url}: status code {response.status_code}""")
                
                    soup = BeautifulSoup(response.text, 'html.parser')

                    anchors = soup.find_all('a')

                    if anchors and len(anchors) > 0 and anchors is not None:

                        # for each of the new urls we find add them to the queue 
                        for i in anchors:
                            
                            # extract url from anchor tag
                            target_url = i.get('href')

                            # exclude any items with https or http to remove external domains
                            # but we dont want to exclude our target domain
                            if target_url is not None and (('https:' not in target_url and 'http:' not in target_url ) or self.domain in target_url) and not any(map(target_url.__contains__, self.exclude)):

                                cleaned_url = self.__clean_url(item.url,target_url)

                                if cleaned_url != "":
                                    #update report with new url
                                    self.__update_report(item.url,cleaned_url)
                                
                                    if cleaned_url not in self.visited_urls and self.page_count <= self.page_limit:
                                        self.visited_urls.append(cleaned_url)

                                        self.url_queue.put(CrawlerQueueItem(cleaned_url,item.depth + 1, item.url))
                                        self.page_count += 1
                                        self.logger.info(f"""Adding {cleaned_url} from {item.url} with depth {item.depth + 1}""")

                except Exception as e:   
                    logging.error(e)
                finally:
                    self.logger.info(f"""done {item.url}""")
                    # mark the item as complete and remove it from the queue
                    self.url_queue.task_done()

            else:
                self.logger.error(f"""Skipping item : {item.url}, max depth of {self.max_depth} exceeded""")
                self.url_queue.task_done()


    def run(self):
        self.__has_ran = False
        self.logger.info("\n")
        self.logger.info(f"""building a queue with {self.workers} workers""")
        self.logger.info("\n")

        self.url_queue = gevent.queue.JoinableQueue()

        # spawn workers
        [gevent.spawn(self.__get_links) for i in range(self.workers)]

        # initialise the queue with the domain
        self.logger.info("\n")
        self.logger.info(f"""starting processing""")
        self.logger.info("\n")
        # add the initial url to the queue
        self.url_queue.put(CrawlerQueueItem(self.domain,0,''))
        self.page_count += 1
        # wait for the queue to be cleared
        self.url_queue.join()

        self.logger.info("\n")
        self.end_time = datetime.datetime.now().timestamp()
        self.logger.info(f"""Discovered {len(self.visited_urls)} urls in {self.end_time - self.start_time}s""")
        self.logger.info("\n")

        self.__has_ran = True
    
    def report(self):
        if self.__has_ran:
            print("\n")
            print(f"""Discovered {len(self.visited_urls)} urls""")
            for i in self.dict_report.keys():
                print("\n")
                print(f"""Page: {i}""")
                for j in self.dict_report[i]:
                    print(f"""{i} => {j}""")
        else:
            print("The crawler has not been run yet!")

    def report_summary(self):
        if self.__has_ran:
            print(f"""{len(self.dict_report.keys())} nodes, {sum(map(len, self.dict_report.values()))} edges\n""")
            print(f"""{self.end_time - self.start_time} seconds""")
        else:
            print("The crawler has not been run yet!")

    def report_status(self):
        if self.__has_ran:
            for i in self.dict_status.keys():
                print(f"status code {i} : {self.dict_status[i]} requests")
        else:
            print("The crawler has not been run yet!")

class CrawlerQueueItem:
    # class to hold info for each of the queue items
    def __init__(self, url:str, depth:int, referrer_url:str):
        self.url = url
        self.depth = depth
        self.referrer_url = referrer_url
