from crawler import Crawler

# instantiate the crawler
my_crawler = Crawler(domain='https://hexus.net/', show_logs=True, workers=1000, max_depth=-1, page_limit=4000, exclude=['.css','.ico','mailto'], retries=5)

# run
my_crawler.run()

# check for any errored requests
# my_crawler.report_status()

# print results to console
my_crawler.report()

# print summary to console
my_crawler.report_summary()

