# Crawler

## Usage (see main.py)

    - Instantiate
    `my_crawler = Crawler('https://news.ycombinator.com', True, 20, -1,['.css','.ico'])`

    - Run
    `my_crawler.run()`

    - Print Report
    `my_crawler.report()`
